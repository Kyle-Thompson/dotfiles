# Use emacs keybindings even if our EDITOR is set to vi.
bindkey -e

# Do not force prompt to start on a newline.
setopt NOPROMPT_CR

