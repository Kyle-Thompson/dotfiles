set -g base-index 1         # starting window numbered 1
set -g pane-base-index 1    # starting pane numbered 1
set -g renumber-windows on  # renumber windows when one is closed

set -g default-shell /bin/zsh  # zsh is the default shell

set -g default-terminal "tmux-256color"
set-option -ga terminal-overrides ",$TERM:rxvt-uni*:Tc"

set -g mouse on
set -sg escape-time 10

# vim style pane selection
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

bind r source-file ~/.tmux.conf

set -g status-left " "
set -g status-right "[#S]"

# colors
set -g mode-bg colour7
set -g mode-fg colour0
set -g status-bg default
set -g status-fg colour7
set -g window-status-format "#W"
set -g window-status-current-format "#[fg=colour4]#W"
set -g pane-active-border-fg colour4
set -g message-bg colour0
set -g message-fg colour7

# system clipboard integration
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xsel -i -b'
bind-key p run 'xsel -o | tmux load-buffer - ; tmux paste-buffer'
bind-key -T copy-mode-vi MouseDragEnd1Pane \
  send-keys -X copy-pipe-and-cancel 'xsel -i -b'

# Double LMB Select & Copy (Word)
bind-key -T copy-mode-vi DoubleClick1Pane \
  select-pane \; \
  send-keys -X select-word \; \
  send-keys -X copy-pipe 'xsel -i -b'
bind-key -n DoubleClick1Pane \
  select-pane \; \
  copy-mode -M \; \
  send-keys -X select-word \; \
  send-keys -X copy-pipe 'xsel -i -b'

# Triple LMB Select & Copy (Line)
bind-key -T copy-mode-vi TripleClick1Pane \
  select-pane \; \
  send-keys -X select-line \; \
  send-keys -X copy-pipe 'xsel -i -b'
bind-key -n TripleClick1Pane \
  select-pane \; \
  copy-mode -M \; \
  send-keys -X select-line \; \
  send-keys -X copy-pipe 'xsel -i -b'
